/**
 * CLBase.h
 * CosteliqueKit
 *
 * Copyright (c) 2019 WebView, Lab.
 * All rights reserved.
 */

#ifndef __CORE_COSTELIQUE_BASE__
#define __CORE_COSTELIQUE_BASE__

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

// The shielding of C code in a C++ project.
#ifdef __cplusplus
	#define CL_EXTERN_C_BEGIN extern "C" {
	#define CL_EXTERN_C_END   }
#else
	#define CL_EXTERN_C_BEGIN
	#define CL_EXTERN_C_END
#endif

// The external function attribute of the library.
#define CL_EXPORT extern

#define CL_NORETURN __attribute__((noreturn))

#define CL_ENUM(_type, _name) _type _name; enum

CL_EXTERN_C_BEGIN

// Polymorphic type for any library object. This type is for library objects only.
// Operations with primitive types or unregistered objects will cause errors and exceptions.
typedef void * CLTypeRef;

// A string constant. Universal replacement of null-terminated 8-bit C-string.
typedef struct __CLString * CLStringRef;

// Meta-class of any library object.
typedef struct __CLRuntimeClass * CLRuntimeClassRef;
typedef unsigned long CLTypeID;

// The definition of the main types of the library.
typedef unsigned char Boolean;
typedef long long CLInteger;
typedef unsigned long long CLUInteger;
typedef const char * UTF8Str;
typedef unsigned char unichar;

// These functions are universal and apply to any library object, as well as to third-party registered classes.
// See CLInternal.h
#pragma mark - Polymorphic functions

/**
 * @abstract Increments the number of object owners.
 * @discussion The library uses the reference count system to manage the life of objects. When the count of
 		references becomes zero, the object is freed from memory. This function increments the number of references
 		to the object.
 * @return A pointer to the passed object. This is done to simplify the structure of the code.
 */
CL_EXPORT
CLTypeRef CLRetain(CLTypeRef object);

/**
 * @abstract Decrements the number of object owners.
 * @discussion The library uses the reference count system to manage the life of objects. When the count of
 		references becomes zero, the object is freed from memory. This function decrements the number of references
 		to the object.
 */
CL_EXPORT
void CLRelease(CLTypeRef object);

/**
 * @abstract Compares for an object to equivalence.
 * @discussion The function takes two library objects as input and compares them. The function immediately returns
 		true if the pointers to the objects are the same, that is, the first and second arguments are the same object.
 		The function immediately returns false if objects of different classes are passed.
 * @return true if the objects are identical and false if they are different. See the description above about
 		exceptional cases.
 */
CL_EXPORT
Boolean CLEqual(CLTypeRef first, CLTypeRef second);

/**
 * @abstract Performs a copy operation on the object.
 * @discussion The function takes an object and performs a copy operation on the object. Note that the copy operation
 		does not affect the contents of the objects. For example, copying an array will not copy the objects it
 		contains, but will only increase the number of references to those objects. For mutable objects (such as
 		NSMutableString) function will return an immutable copy, CLCopy(NSMutableStringRef) = CLStringRef.
 * @return Pointer to the object created as a result of copying.
 */
CL_EXPORT
CLTypeRef CLCopy(CLTypeRef object);

/**
 * @abstract Performs a copy operation on the object.
 * @discussion The function takes an object and performs a copy operation on the object. Note that the copy operation
 		does not affect the contents of the objects. For example, copying an array will not copy the objects it contains,
 		but will only increase the number of references to those objects. The function always returns a mutable copy of
		the object. For the CLStringRef class, the function returns the CLMutableStringRef class.
 
 		Note that not all objects have editable variants. Calling this function on an object that does not have a
 		modifiable variant will throw an exception.
 
 * @return A mutable variant of the passed object.
 */
CL_EXPORT
CLTypeRef CLMutableCopy(CLTypeRef object);

/**
 * @abstract Returns the description of the object as a CLStringRef.
 * @discussion Each library object has a description that simplifies the process of writing code and debugging. Calling
		this function allows you to get the description of the object as a CLStringRef string.
 * @return Object description.
 */
CL_EXPORT
CLStringRef CLDescription(CLTypeRef object);


// These functions allow you to compare objects with each other, manage, and identify polymorphic objects.
#pragma mark - Object management

/**
 * @abstract Returns the unique identifier of the library object.
 * @discussion Any library class has its own unique identifier (which must also have any third-party class).
 		The function allows you to get a unique identifier of the object and uniquely identify the object.
 */
CL_EXPORT
CLTypeID CLGetTypeID(CLTypeRef object);

/**
 * @abstract Returns a pointer to an object class.
 * @discussion Any library object has a class that stores all information about the object, as well as pointers to
 		polymorphic functions that each object implements. The function returns a pointer to such a class. Be careful
 		not to try to change this class, it will lead to runtime errors.
 */
CL_EXPORT
CLRuntimeClassRef CLGetRuntimeClass(CLTypeRef object);

/**
 * @abstract Checks if the object belongs to a class.
 * @discussion Any library object has a class that stores all information about the object, as well as pointers to
 		polymorphic functions that each object implements. This function checks if the object belongs to a class.
 * @return true if the object belongs to the class and false if it does not.
 */
CL_EXPORT
Boolean CLHasMemberOfClass(CLTypeRef object, CLRuntimeClassRef runtimeClass);

CL_EXTERN_C_END

#endif /* __CORE_COSTELIQUE_BASE__ */
