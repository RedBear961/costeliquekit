/**
 * CLStringTokenizer.h
 * CosteliqueKit
 *
 * Copyright (c) 2019 WebView, Lab.
 * All rights reserved.
 */

#ifndef __CORE_COSTELIQUE_STRING_TOKENIZER__
#define __CORE_COSTELIQUE_STRING_TOKENIZER__

#include <CosteliqueKit/CLBase.h>

#endif /* __CORE_COSTELIQUE_STRING_TOKENIZER__ */
