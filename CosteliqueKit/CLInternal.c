/*
 * CLInternal.c
 * CosteliqueKit
 *
 * Copyright (c) 2019 WebView, Lab.
 * All rights reserved.
 */

#include "CLInternal.h"
#include "CLRuntime.h"

// A function that throws exceptions if something goes wrong.
void CLNoCosteliqueKitObjectException(CLTypeRef object, UTF8Str functionName) CL_NORETURN;
void CLNullPointerException(UTF8Str functionName) CL_NORETURN;

#pragma mark - Class control

// Table of the main classes of the library. It is used to validate objects.
static const CLRuntimeClass * const CLRuntimeClassTable[CLRuntimeClassTableSize] =
{
	[_kCLRuntimeClassIDString] = &CLRuntimeClassString,
	[_kCLRuntimeClassIDStringTokenizer] = &CLRuntimeClassStringTokenizer,
	[_kCLRuntimeClassIDArray] = &CLRuntimeClassArray,
	[_kCLRuntimeClassIDDictionary] = &CLRuntimeClassDictionary,
	[_kCLRuntimeClassIDNumber] = &CLRuntimeClassNumber,
	[_kCLRuntimeClassIDData] = &CLRuntimeClassData,
	[_kCLRuntimeClassIDDate] = &CLRuntimeClassDate,
	[_kCLRuntimeClassIDNotificationCenter] = &CLRuntimeClassNotificationCenter,
	[_kCLRuntimeClassIDNotification] = &CLRuntimeClassNotification,
	[_kCLRuntimeClassIDIndexSet] = &CLRuntimeClassIndexSet,
	[_kCLRuntimeClassIDCharacterSet] = &CLRuntimeClassCharacterSet,
	[_kCLRuntimeClassIDSet] = &CLRuntimeClassSet,
	[_kCLRuntimeClassIDEnumerator] = &CLRuntimeClassEnumerator
};

Boolean CLValidateClass(CLRuntimeClassRef class)
{
	// Gets the ID of the class that will access the table.
	// Returns false immediately if ID is not in the range of table indexes.
	CLTypeID id = class->uniqueID;
	if (id < CLRuntimeClassTableSize)
	{
		// Because all objects own the same pointer, compares the pointers.
		CLRuntimeClassRef tableClass = (CLRuntimeClassRef)CLRuntimeClassTable[id];
		return class == tableClass;
	}
	
	return false;
}

CLRuntimeClassRef _CLGetRuntimeClass(CLTypeRef object, UTF8Str callFunction)
{
	// Throws an exception if NULL is passed.
	if (!object) CLNullPointerException(callFunction);
	
	// Tries to select a class from an object.
	CLRuntimeClass *class = (*(void **)object);
	
	if (class && CLValidateClass(object))
		return class;
	
	// Throws an exception if the class is not registered.
	CLNoCosteliqueKitObjectException(object, callFunction);
}

CLTypeID _CLGetTypeID(CLTypeRef object, UTF8Str callFunction)
{
	// Tries to select a class from an object.
	// Throws an exception if the object has no class or class is not registered.
	CLRuntimeClass *class = _CLGetRuntimeClass(object, callFunction);
	return class->uniqueID;
}

void CLNoCosteliqueKitObjectException(CLTypeRef object, UTF8Str functionName)
{
	// Displays a message about the use of non-CL object and aborts the program.
	printf("Attempting to call function '%s' to object '%p', which is not CosteliqueKit object or was not registered.",
		   functionName, object);
	abort();
}

void CLNullPointerException(UTF8Str functionName)
{
	// Displays a message that the null argument is invalid and aborts the program.
	printf("An attempt to call function '%s' for a null argument that cannot be null.",
		   functionName);
	abort();
}


#pragma mark - Polymorphic functions

CLTypeRef CLRetain(CLTypeRef object)
{
	// Tries to select a class from an object.
	// Throws an exception if the object has no class or class is not registered.
	CLRuntimeClass *class = _CLGetRuntimeClass(object, "CLRetain()");
	return class->basic.CLRetain(object);
}

void CLRelease(CLTypeRef object)
{
	// Tries to select a class from an object.
	// Throws an exception if the object has no class or class is not registered.
	CLRuntimeClass *class = _CLGetRuntimeClass(object, "CLRelease()");
	return class->basic.CLRelease(object);
}

Boolean CLEqual(CLTypeRef first, CLTypeRef second)
{
	// Validates the class by searching the class table.
	CLRuntimeClass *firstClass = _CLGetRuntimeClass(first, "CLEqual()");
	CLRuntimeClass *secondClass = _CLGetRuntimeClass(second, "CLEqual()");
	
	// If objects of different classes are compared, it returns false immediately.
	if (firstClass != secondClass)
		return false;
	
	// If the pointers are equal immediately returns true.
	if (first == second)
		return true;
	
	return firstClass->basic.CLEqual(first, second);
}

CLTypeRef CLCopy(CLTypeRef object)
{
	// Tries to select a class from an object.
	// Throws an exception if the object has no class or class is not registered.
	CLRuntimeClass *class = _CLGetRuntimeClass(object, "CLCopy()");
	return class->basic.CLCopy(object);
}

CLTypeRef CLMutableCopy(CLTypeRef object)
{
	// Tries to select a class from an object.
	// Throws an exception if the object has no class or class is not registered.
	CLRuntimeClass *class = _CLGetRuntimeClass(object, "CLMutableCopy()");
	return class->basic.CLMutableCopy(object);
}

CLStringRef CLDescription(CLTypeRef object)
{
	// Tries to select a class from an object.
	// Throws an exception if the object has no class or class is not registered.
	CLRuntimeClass *class = _CLGetRuntimeClass(object, "CLDescription()");
	return class->basic.CLDescription(object);
}

CLTypeID CLGetTypeID(CLTypeRef object)
{
	// Calls the general function to get the ID.
	return _CLGetTypeID(object, "CLGetTypeID()");
}

CLRuntimeClassRef CLGetRuntimeClass(CLTypeRef object)
{
	// Calls the general function to get the runtime class.
	return _CLGetRuntimeClass(object, "CLGetRuntimeClass()");
}

Boolean CLHasMemberOfClass(CLTypeRef object, CLRuntimeClassRef runtimeClass)
{
	// Gets the object class. Returns false if the object belongs to another class.
	CLRuntimeClass *objectClass = _CLGetRuntimeClass(object, "CLHasMemberOfClass()");
	if (objectClass != runtimeClass)
		return false;
	
	// Pulls a class function.
	return runtimeClass->basic.CLHasMember(object);
}
