/**
 * CosteliqueKit.h
 * CosteliqueKit
 *
 * Copyright (c) 2019 WebView, Lab.
 * All rights reserved.
 */

#ifndef __COSTELIQUE_KIT__
#define __COSTELIQUE_KIT__

// Base class.
#include <CosteliqueKit/CLBase.h>
#include <CosteliqueKit/CLString.h>
#include <CosteliqueKit/CLStringTokenizer.h>
#include <CosteliqueKit/CLArray.h>
#include <CosteliqueKit/CLDictionary.h>
#include <CosteliqueKit/CLNumber.h>
#include <CosteliqueKit/CLData.h>
#include <CosteliqueKit/CLDate.h>
#include <CosteliqueKit/CLNotificationCenter.h>
#include <CosteliqueKit/CLNotification.h>
#include <CosteliqueKit/CLRange.h>
#include <CosteliqueKit/CLIndexSet.h>
#include <CosteliqueKit/CLCharacterSet.h>
#include <CosteliqueKit/CLSet.h>
#include <CosteliqueKit/CLEnumerator.h>

// Logic and the extension libraries. Use with caution!
#include <CosteliqueKit/CLInternal.h>
#include <CosteliqueKit/CLRuntime.h>

#endif /* __COSTELIQUE_KIT__ */
