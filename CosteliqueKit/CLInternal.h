/**
 * CLInternal.h
 * CosteliqueKit
 *
 * Copyright (c) 2019 WebView, Lab.
 * All rights reserved.
 */

#ifndef __CORE_COSTELIQUE_INTERNAL__
#define __CORE_COSTELIQUE_INTERNAL__

#include <CosteliqueKit/CLBase.h>

CL_EXTERN_C_BEGIN

// A class version string, such as "1.2.5".
typedef UTF8Str CLVersion;

// A block of pointers to polymorphic functions implements each object.
typedef struct __CLClassBasic
{
	CLTypeRef (*CLRetain)(CLTypeRef);
	void (*CLRelease)(CLTypeRef);
	Boolean (*CLEqual)(CLTypeRef, CLTypeRef);
	CLTypeRef (*CLCopy)(CLTypeRef);
	CLTypeRef (*CLMutableCopy)(CLTypeRef); // Not all objects implement it.
	CLStringRef (*CLDescription)(CLTypeRef); // CLStringRef (CLMutableStringRef) call inside the function CLCopy().
	
	Boolean (*CLHasMember)(CLTypeRef);
} CLClassBasic;

typedef struct __CLRuntimeClass
{
	UTF8Str className; // Unique class name.
	CLTypeID uniqueID; // The unique identifier of the class.
	CLVersion versionString; // Version string, see type definition at the top.
	CLClassBasic basic; // Block of polymorphic functions.
} CLRuntimeClass;

CL_EXTERN_C_END

#endif /* __CORE_COSTELIQUE_INTERNAL__ */
