/**
 * CLString.h
 * CosteliqueKit
 *
 * Copyright (c) 2019 WebView, Lab.
 * All rights reserved.
 */

#ifndef __CORE_COSTELIQUE_STRING__
#define __CORE_COSTELIQUE_STRING__

#include <CosteliqueKit/CLBase.h>

#endif /* __CORE_COSTELIQUE_STRING__ */
