/**
 * CLRuntime.h
 * CosteliqueKit
 *
 * Copyright (c) 2019 WebView, Lab.
 * All rights reserved.
 */

#ifndef __CORE_COSTELIQUE_RUNTIME__
#define __CORE_COSTELIQUE_RUNTIME__

#include <CosteliqueKit/CLBase.h>
#include <CosteliqueKit/CLInternal.h>

CL_EXTERN_C_BEGIN

// The number of objects in the class table.
#define CLRuntimeClassTableSize 13

// A listing of class identifiers. It is also used in the class table.
typedef CL_ENUM(CLInteger, CLRuntimeClassID)
{
	_kCLRuntimeClassIDString = 0,
	_kCLRuntimeClassIDStringTokenizer = 1,
	_kCLRuntimeClassIDArray = 2,
	_kCLRuntimeClassIDDictionary = 3,
	_kCLRuntimeClassIDNumber = 4,
	_kCLRuntimeClassIDData = 5,
	_kCLRuntimeClassIDDate = 6,
	_kCLRuntimeClassIDNotificationCenter = 7,
	_kCLRuntimeClassIDNotification = 8,
	_kCLRuntimeClassIDIndexSet = 9,
	_kCLRuntimeClassIDCharacterSet = 10,
	_kCLRuntimeClassIDSet = 11,
	_kCLRuntimeClassIDEnumerator = 12,
};

// External class variables. Classes implementing the objects inside their files.
CL_EXPORT CLRuntimeClass const CLRuntimeClassString;
CL_EXPORT CLRuntimeClass const CLRuntimeClassStringTokenizer;
CL_EXPORT CLRuntimeClass const CLRuntimeClassArray;
CL_EXPORT CLRuntimeClass const CLRuntimeClassDictionary;
CL_EXPORT CLRuntimeClass const CLRuntimeClassNumber;
CL_EXPORT CLRuntimeClass const CLRuntimeClassData;
CL_EXPORT CLRuntimeClass const CLRuntimeClassDate;
CL_EXPORT CLRuntimeClass const CLRuntimeClassNotificationCenter;
CL_EXPORT CLRuntimeClass const CLRuntimeClassNotification;
CL_EXPORT CLRuntimeClass const CLRuntimeClassIndexSet;
CL_EXPORT CLRuntimeClass const CLRuntimeClassCharacterSet;
CL_EXPORT CLRuntimeClass const CLRuntimeClassSet;
CL_EXPORT CLRuntimeClass const CLRuntimeClassEnumerator;

CL_EXTERN_C_END

#endif /* __CORE_COSTELIQUE_RUNTIME__ */
