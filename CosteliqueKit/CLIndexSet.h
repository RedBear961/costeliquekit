/**
 * CLIndexSet.h
 * CosteliqueKit
 *
 * Copyright (c) 2019 WebView, Lab.
 * All rights reserved.
 */

#ifndef __CORE_COSTELIQUE_INDEX_SET__
#define __CORE_COSTELIQUE_INDEX_SET__

#include <CosteliqueKit/CLBase.h>

#endif /* __CORE_COSTELIQUE_INDEX_SET__ */
